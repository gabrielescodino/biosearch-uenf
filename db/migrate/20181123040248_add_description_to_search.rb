class AddDescriptionToSearch < ActiveRecord::Migration[5.2]
  def change
    add_column :searches, :description, :jsonb, null: false, default: '{}'
  end
end
