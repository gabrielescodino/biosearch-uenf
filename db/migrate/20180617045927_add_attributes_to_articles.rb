class AddAttributesToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :title, :string
    add_column :articles, :abstract, :text
    add_column :articles, :pmid, :string
    add_column :articles, :identification_token, :string
    add_column :articles, :term, :string    
  end
end
