class AddAttributesToKeywords < ActiveRecord::Migration[5.2]
  def change
    add_column    :keywords, :name, :string
    add_column    :keywords, :weight, :float
    add_reference :keywords, :article
  end
end
