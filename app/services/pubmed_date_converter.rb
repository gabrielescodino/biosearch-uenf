module PubmedDateConverter
  class << self
    def convert(hash_date)
      return nil if hash_date.nil?
      if hash_date['DateType'] == 'Electronic'
        "#{hash_date['Day']}/#{hash_date['Month']}/#{hash_date['Year']}".to_date
      else
        nil
      end
    end
  end
end
