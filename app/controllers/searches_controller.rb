require 'rexml/document'
include REXML
require 'open-uri'

class SearchesController < ApplicationController
  def index
    @search_form = SearchForm.new(q: params[:q])
    @articles = build_my_articles
  end

  def build_my_articles
    per_page = 100
    clear_articles

    if params[:q].present?
      clear_search
      params[:q].delete('')
      term = I18n.transliterate(params[:q].join(' '))
      term = term.tr(' ', '+')

      Eutils::Esearch.articles_pmids_about(term)

      ids = Search.first.description

      paginated_ids = ids[0..per_page]
      Eutils::Efetch.articles_about(paginated_ids, term)

      return if Article.count.zero?

      max_concept_length = params[:search_form][:max_concept_length].to_i || 5

      Article.find_each do |article|
        article.create_keywords!(max_concept_length)
      end

      @suggestions = Keyword.hash_of_grouped_with_weights.first(20).map { |item| item[0] }
    elsif params[:page].present?
      ids = Search.first.description

      paginated_ids = ids[((params[:page].to_i.presence || 0) * per_page)..(params[:page].to_i.presence + 1) * per_page]

      Eutils::Efetch.articles_about(paginated_ids, term)

      return if Article.count.zero?

      max_concept_length = 5

      Article.find_each do |article|
        article.create_keywords!(max_concept_length)
      end

      @suggestions = Keyword.hash_of_grouped_with_weights.first(20).map { |item| item[0] }
    end
    Article.all
  end

  private

  def clear_articles
    Keyword.delete_all
    Article.delete_all
  end

  def clear_search
    Search.delete_all
  end
end
