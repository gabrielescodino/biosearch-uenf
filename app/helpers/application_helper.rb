module ApplicationHelper
  def link_to_with_pagination(text, page)
    link_to text, searches_path(page: page)
  end
end
