class SearchForm
  include ActiveModel::Model

  attr_accessor :q, :max_concept_length

  def initialize(args)
    @q = (args[:q] || []).reject(&:blank?)
    @max_concept_length = args[:max_concept_length]
  end
end
