$(function () {
  $(document).ready(function(){
    $(".select2-tokenizer").select2({
  		placeholder: "Search", //placeholder
  		tags: true,
  		tokenSeparators: ['/',',',';']
  	})

    draggableComponent();
  });
});

var draggableComponent = (function () {
  // Cache DOM
  $select = $('.select2-tokenizer');

  function _buildData (element) {
    return {
      id:   element.data('value'),
      text: element.data('value')
    };
  }

  function _buildOption (element) {
    var data = _buildData (element)
    return new Option(data.text, data.id, true, true);
  }

  function _onDrop (event, ui) {
    var newOption = _buildOption(ui.draggable)

    $select.append(newOption).trigger('change');
    ui.draggable.remove();
  }

  // Start binds
  $('[data-btn-searchable]').draggable();
  $(".select2").droppable({
    drop: _onDrop
  });

})
