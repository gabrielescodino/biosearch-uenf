# frozen_string_literal: true

# noDoc
class Keyword < ActiveRecord::Base
  # :: Associations
  belongs_to :article, required: true

  # :: Methods
  def self.hash_of_grouped_with_weights
    grouped_keywords = Keyword.all.group_by(&:name)

    foo_hash = {}

    grouped_keywords.each do |key, value|
      foo_hash[key] = value.map(&:weight).sum
    end

    foo_hash.sort_by { |_key, value| value }.reverse
  end
end
