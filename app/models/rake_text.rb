# frozen_string_literal: true

# Rake algorithm
class RakeText
  @stoplist_fox =
  %w[a about above across after again against all almost alone
     along already also although always among an and another any anybody anyone
     anything anywhere are area areas around as ask asked asking asks at away b
     back backed backing backs be because became become becomes been before began
     behind being beings best better between big both but by c came can cannot
     case cases certain certainly clear clearly come could d did differ different
     differently do does done down downed downing downs during e each early either
     end ended ending ends enough even evenly ever every everybody everyone
     everything everywhere f face faces fact facts far felt few find finds first
     for four from full fully further furthered furthering furthers g gave
     general generally get gets give given gives go going good goods got great
     greater greatest group grouped grouping groups h had has have having he her
     herself here high higher highest him himself his how however i if important
     in interest interested interesting interests into is it its itself j just k
     keep keeps kind knew know known knows l large largely last later latest
     least less let lets like likely long longer longest m made make making man
     many may me member members men might more most mostly mr mrs much must my
     myself n necessary need needed needing needs never new newer newest next no
     non not nobody noone nothing now nowhere number numbered numbering numbers o
     of off often old older oldest on once one only open opened opening opens or
     order ordered ordering orders other others our out over p part parted
     parting parts per perhaps place places point pointed pointing points
     possible present presented presenting presents problem problems put puts q
     quite r rather really right room rooms s said same saw say says second
     seconds see seem seemed seeming seems sees several shall she should show
     showed showing shows side sides since small smaller smallest so some
     somebody someone something somewhere state states still such sure t take
     taken than that the their them then there therefore these they thing things
     think thinks this those though thought thoughts three through thus to today
     together too took toward turn turned turning turns two u under until up upon
     us use uses used v very w want wanted wanting wants was way ways we well
     wells went were what when where whether which while who whole whose why will
     with within without work worked working works would x y year years yet you
     young younger youngest your yours z]

  def self.FOX
    @stoplist_fox
  end

  def initialize; end

	def analyse text, stoplist, verbose=false
		pattern    = buildStopwordRegExPattern stoplist
		sentences  = text.split(/[.!?,;:\t\\-\\"\\(\\)\\\'\u2019\u2013]/u)
		phrases    = generateCandidateKeywords sentences, pattern
		wordscores = calculateWordScores phrases
		candidates = generate_candidate_keyword_scores phrases, wordscores

		if verbose == true
			result = candidates.sort_by{|k,v| v}.reverse
			result.each do |word, score|
				puts sprintf '%.2f - %s', score, word
			end
		end

		return candidates
	end

	private

	# create stopword pattern
	# 1
	def buildStopwordRegExPattern words
		pattern = Array.new
		words.each do |word|
			pattern.push '\\b'+word+'\\b'
		end
		return Regexp.new(pattern.join("|"), Regexp::IGNORECASE)
	end

	# generate candidate keywords
	# 2
	def generateCandidateKeywords sentences, pattern
		phrases = Array.new

		sentences.each do |sentence|
			sentence = sentence.strip

			tmp = sentence.gsub pattern, "|"

			tmp.split("|").each do |part|
				part = part.strip.downcase
				if !part.empty?
					phrases.push part
				end
			end
		end

		return phrases
	end

	# calculate individual word scores
	# 3
	def calculateWordScores phrases
		word_freq = Hash.new 0
		word_degree = Hash.new 0
		word_score = Hash.new 0

		phrases.each do |phrase|
			words = seperateWords phrase

			length = words.length
			degree = length-1

			words.each do |word|
				word_freq[word] += 1
				word_degree[word] += degree
			end
		end

		word_freq.each do |word, counter|
			word_degree[word] = word_degree[word] + word_freq[word]
		end

		word_freq.each do |word, counter|
			word_score[word] = word_degree[word]/(word_freq[word] * 1.0)
		end

		return word_score
	end

	# generate candidate keyword scores
	# 4
	def generate_candidate_keyword_scores phrases, scores
		candidates = Hash.new 0

		phrases.each do |phrase|
			words = seperateWords(phrase)
			score = 0
			words.each do |word|
				score += scores[word]
			end
			candidates[phrase] = score
		end

		return candidates
	end

	def seperateWords text
		words = Array.new

		text.split(/[^a-zA-Z0-9_\\+\\-]/).each do |word|
			word = word.strip.downcase
			if !word.empty? && !(true if Float(word) rescue false)
				words.push word
			end
		end

		return words
	end
end
