# frozen_string_literal: true

# noDoc
class Article < ActiveRecord::Base
  # :: Associations
  has_many :keywords, dependent: :destroy

  # :: Validations
  validates :pmid, presence: true

  # :: Methods
  def concat_title_and_abstract
    title + ' ' + abstract
  end

  def create_keywords!(max_concept_length)
    text = concat_title_and_abstract

    #rake = RakeText.new
    #concepts = rake.analyse text, RakeText.FOX, true

    result = RakeNLP.run(text, {
      min_phrase_length: 1,
      max_phrase_length: max_concept_length,
      min_frequency:     1,
      min_score:         1,
      stop_list:         RakeNLP::StopList.new
    })

    concepts_size = (result.keywords.size / 3.0).ceil
    concepts = result.keywords.first(concepts_size).to_h

    concepts.each do |key, value|
      keyword = Keyword.new(article: self, name: key, weight: value)
      keyword.save!
    end
  end
end
