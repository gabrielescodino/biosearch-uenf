class Eutils::Esearch
  def self.url(term)
    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmax=10000&term="+ term +
    "+AND+(Review%5Bptyp%5D+AND+%22loattrfree+full+text%22%5Bsb%5D)&usehistory=y"
  end

  def self.articles_pmids_about(term)
    xmlfile = open(Eutils::Esearch.url(term))
    xmldoc = Document.new(xmlfile)

    ids = []
    xmldoc.elements.each("eSearchResult/IdList/Id") do |e|
      ids << e.text.to_i
    end
    Search.create!(description: ids)
    ids
  end
end
