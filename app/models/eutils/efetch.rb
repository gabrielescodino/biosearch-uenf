class Eutils::Efetch
  def self.url(article_ids_list)
    articles_ids = article_ids_list.join(',')
    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?retmode=xml&db=pubmed&id=" + articles_ids
  end

  def self.articles_about(article_ids_list, term)
    xml_content = Net::HTTP.get(URI.parse(Eutils::Efetch.url(article_ids_list)))
    hash_articles = Hash.from_xml(xml_content)

    return unless hash_articles['PubmedArticleSet'].present?
    pubmed_articles = hash_articles['PubmedArticleSet']['PubmedArticle']

    pubmed_articles.each do |element|
      pmid     = element['MedlineCitation']['PMID']
      abstract = medline_citation_article_abstract(element)
      title    = element['MedlineCitation']['Article']['ArticleTitle']
      date     = ::PubmedDateConverter.convert(element['MedlineCitation']['Article']['ArticleDate'])
      term     = term

      article = Article.new(pmid: pmid, abstract: abstract, title: title, term: term, date: date)
      article.save!
    end
  end

  def self.medline_citation_article_abstract(element)
    if element['MedlineCitation']['Article'].key?('Abstract')
      element['MedlineCitation']['Article']['Abstract']['AbstractText']
    else
      ''
    end
  end
end
